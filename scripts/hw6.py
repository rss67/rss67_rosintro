from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg


try:
    from math import pi, tau, dist, fabs, cos
except:  
    from math import pi, fabs, cos, sqrt
    tau = 2.0 * pi


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list


rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

robot =moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()

group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

display_trajectory_publisher = rospy.Publisher(
"/move_group/display_planned_path",
moveit_msgs.msg.DisplayTrajectory,
queue_size=20,
)

# We can get the name of the reference frame for this robot:
planning_frame = move_group.get_planning_frame()
print("============ Planning frame: %s" % planning_frame)

# We can also print the name of the end-effector link for this group:
eef_link = move_group.get_end_effector_link()
print("============ End effector link: %s" % eef_link)

# We can get a list of all the groups in the robot:
group_names = robot.get_group_names()
print("============ Available Planning Groups:", robot.get_group_names())

# Sometimes for debugging it is useful to print the entire state of the
# robot:
print("============ Printing robot state")
print(robot.get_current_state())
print("")


joint_goal = move_group.get_current_joint_values()
joint_goal[0] = tau/2
joint_goal[1] = -0.316*tau
joint_goal[2] = -0.23888*tau
joint_goal[3] = -0.0833*tau
joint_goal[4] = -0.25*tau
joint_goal[5] = -0.25*tau

'''
joint_goal[0] = tau/2
joint_goal[1] = -0.316*tau
joint_goal[2] = 0.344*tau
joint_goal[3] = -0.08333*tau
joint_goal[4] = 0.25*tau
joint_goal[5] = 0.25*tau
'''

# The go command can be called with joint values, poses, or without any
# parameters if you have already set the pose or joint target for the group
move_group.go(joint_goal, wait=True)

# Calling ``stop()`` ensures that there is no residual movement
move_group.stop()

#Function for Pose Goal.
def moveViaPose(w, x, y, z):

    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.orientation.w = w
    pose_goal.orientation.x = 0
    pose_goal.orientation.y = 0
    pose_goal.orientation.z = 0
    pose_goal.position.x = x
    pose_goal.position.y = y
    pose_goal.position.z = z
    move_group.set_pose_target(pose_goal)
    success = move_group.go(wait=True)

    move_group.stop()

    move_group.clear_pose_targets()



#Writing "R"
moveViaPose(1, 0.511952, -0.0988222, 0.239254)
moveViaPose(1, 0.871601, -0.0988222, 0.239254)
moveViaPose(1, 0.7746793, -0.-0.302157, 0.239254)
moveViaPose(1, 0.680738, -0.0988222, 0.239254)
moveViaPose(1, 0.511952, -0.302157, 0.239254)

#Transitioning from first to second letter
""" moveViaPose(1, 0.511952, -0.302157, 0.329832)
moveViaPose(1, 0.641347, -0.624331, 0.329832)
moveViaPose(1, 0.641347, -0.624331, 0.239254) """

#Writing "S"
moveViaPose(1, 0.695696, -0.426407, 0.239254)
moveViaPose(1, 0.695696, -0.028184, 0.239254)
moveViaPose(1, 0.62374, -0.028184, 0.239254)
moveViaPose(1, 0.62374, -0.426407, 0.239254)
moveViaPose(1, 0.55374, -0.426407, 0.239254)
moveViaPose(1, 0.55374, -0.028184, 0.239254)



#Stop residual movement
move_group.stop()

""" 
def main():
   initialize()
   reset_joint_angles()

   draw_R()
   reset_joint_angles()

   draw_S()
   reset_joint_angles()

   draw_H()
   reset_joint angles()

   move_group.stop()
 """

