#import important libraries such as MoveIt and Rospy

from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

# import math properties for angle calculations
try:
    from math import pi, tau, dist, fabs, cos
except:  
    from math import pi, fabs, cos, sqrt
    tau = 2.0 * pi


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list



#create class for robotic movement to create the initials R S H
class RSH(object):

    def __init__(self):
        super(RSH, self).__init__()

        #initialize  rospy node
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

        #Create RobotCommander object, used to track position and joint info
        robot =moveit_commander.RobotCommander()

        #Create PlanningSceneInterface object in order to access info about robot's workspace and enviroment
        scene = moveit_commander.PlanningSceneInterface()

        #Create MoveGroupCommander object to interface with joints of UR5e
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        #Creates a publisher for RVIZ visualization 
        display_trajectory_publisher = rospy.Publisher(
        "/move_group/display_planned_path",
        moveit_msgs.msg.DisplayTrajectory,
        queue_size=20,
        )

        #Print important information to terminal on startup

        # name of the reference frame for this robot:
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        #  print the name of the end-effector link for this group:
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        #  list of all the groups in the robot:
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())

        # Sometimes for debugging it is useful to print the entire state of the
        # robot:
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")

        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names
    
    # Function resets joint angles to starting position
    # Prevents singularities and maintains movement in workspace
    # Joint angles found via trial and error using RVIZ "Joints" tab
    def reset_joint_angles(self):
        move_group = self.move_group
        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = tau/2
        joint_goal[1] = -0.316*tau
        joint_goal[2] = -0.23888*tau
        joint_goal[3] = -0.18333*tau
        joint_goal[4] = -0.25*tau
        joint_goal[5] = -0.25*tau

        # The go command can be called with joint values, poses, or without any
        # parameters if you have already set the pose or joint target for the group
        move_group.go(joint_goal, wait=True)

        # Calling ``stop()`` ensures that there is no residual movement
        move_group.stop()


    # Funtion facilitates movement by PoseGoal --> R and S initial
    # Orientation remains static, while translation changes
    def moveViaPose(self, x, y, z):
        move_group = self.move_group
        
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = 1
        pose_goal.orientation.x = 0
        pose_goal.orientation.y = 0
        pose_goal.orientation.z = 0
        pose_goal.position.x = x
        pose_goal.position.y = y
        pose_goal.position.z = z
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)

        move_group.stop()

        move_group.clear_pose_targets() 

    # Drawing 'R' using PoseGoal
    def drawR(self):
        move_group = self.move_group
        self.moveViaPose(0.511952, -0.0988222, 0.239254)
        self.moveViaPose(0.871601, -0.0988222, 0.239254)
        pos=move_group.get_current_joint_values()
        print(pos) #printing position for Pose 1 for numerical analysis
        self.moveViaPose(0.7746793, -0.-0.302157, 0.239254)
        self.moveViaPose(0.680738, -0.0988222, 0.239254)
        self.moveViaPose(0.511952, -0.302157, 0.239254)

    

    # Drawing 'S' using PoseGoal
    def drawS(self):
        move_group = self.move_group
        self.moveViaPose(0.695696, -0.426407, 0.239254)
        self.moveViaPose(0.695696, -0.028184, 0.239254)
        pos=move_group.get_current_joint_values()
        print(pos) #printing position for Pose 2 for numerical analysis
        self.moveViaPose(0.62374, -0.028184, 0.239254)
        self.moveViaPose(0.62374, -0.426407, 0.239254)
        self.moveViaPose(0.55374, -0.426407, 0.239254)
        self.moveViaPose(0.55374, -0.028184, 0.239254)

    # Drawing 'H' using PoseGoal
    def drawH(self, scale=1):
        
        move_group = self.move_group


        self.moveViaPose(0.511952, -0.0988222, 0.239254)
        self.moveViaPose(0.771601, -0.0988222, 0.239254)
        self.moveViaPose(0.593601, -0.0988222, 0.239254)
        self.moveViaPose(0.593601, -0.400, 0.239254)
        self.moveViaPose(0.771601, -0.4, 0.239254)
        pos=move_group.get_current_joint_values()
        print(pos) #printing position for Pose 3 for numerical analysis
        self.moveViaPose(0.511952, -0.4, 0.239254)

      
    
    #Used for cartesian plan, but not used for this code
    def execute_plan(self, plan):
        
        move_group = self.move_group

        move_group.execute(plan, wait=True)

#Main code 
def main():
    try:
        move = RSH()

        #root position
        move.reset_joint_angles()
       
        move.drawR()    #draw R initial
        #move.reset_joint_angles()  #root position

        move.drawS()    #draw S
        #move.reset_joint_angles()  #root position 


        move.drawH() #draw H
        move.reset_joint_angles() #root position """


        print("============ Tracing complete!")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return

#call main function --> run code
if __name__ == "__main__":
    main()
