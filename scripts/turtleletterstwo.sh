rosservice call /kill 'turtle1'
rosservice call /spawn '7' '6' '0' 'turtle2'
rosservice call /spawn '3' '6' '0' 'turtle1'


rosservice call /turtle1/set_pen "{r: 100, g: 0, b: 0, width: 0, 'off': 0}"
rosservice call /turtle2/set_pen "{r: 0, g: 100, b: 0, width: 0, 'off': 0}"


rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[0,5 ,0]' '[0, 0, 0]'
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[4,0 ,0]' '[0, 0,-3]'
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[0,3 ,0]' '[0, 0, 1.0]'


rostopic pub -1 turtle2/cmd_vel geometry_msgs/Twist -- '[0,7 ,0]' '[0, 0, -9.0]'
rostopic pub -1 turtle2/cmd_vel geometry_msgs/Twist -- '[0,1.5 ,0]' '[0, 0, 1.0]'
